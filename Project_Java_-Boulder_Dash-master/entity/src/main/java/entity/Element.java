/*
 * 
 */
package entity;

// TODO: Auto-generated Javadoc
/**
 * The Class Element.
 */
public class Element {

	/** The sprite. */
	private String sprite;

	/**
	 * Instantiates a new element.
	 *
	 * @param sprite the sprite
	 */
	public Element(String sprite) {
		this.sprite = sprite;
	}

	/**
	 * Gets the sprite.
	 *
	 * @return the sprite
	 */
	public String getSprite() {
		return this.sprite;
	}

}

/*
 * 
 */
package entity;

// TODO: Auto-generated Javadoc
/**
 * The Class Player.
 */
public class Player extends Element {

	/** The x. */
	private int x;

	/** The y. */
	private int y;

	/** The alive. */
	private boolean alive = true;

	/** The sprite. */
	private static String sprite = "PLAYER.jpg";

	/**
	 * Instantiates a new player.
	 */
	private Player() {
		super(Player.sprite);

	}

	/**
	 * Move up.
	 *
	 * @param y the y
	 */
	public void moveUp(int y) {
		this.y = this.y + 1;
	}

	/**
	 * Move left.
	 *
	 * @param x the x
	 */
	public void moveLeft(int x) {
		this.x = this.x - 1;
	}

	/**
	 * Move right.
	 *
	 * @param x the x
	 */
	public void moveRight(int x) {
		this.x = this.x + 1;
	}

	/**
	 * Move down.
	 *
	 * @param y the y
	 */
	public void moveDown(int y) {
		this.y = this.y - 1;
	}

	/**
	 * Dead.
	 */
	public void dead() {

	}

}

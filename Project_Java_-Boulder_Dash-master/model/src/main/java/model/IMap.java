/*
 * 
 */
package model;

// TODO: Auto-generated Javadoc
/**
 * The Interface IMap.
 */
public interface IMap {

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	int getWidth();

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	int getHeight();

	/**
	 * Spawn exit.
	 */
	void spawnExit();

}
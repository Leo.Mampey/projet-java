/*
 *
 */
package model;

import java.sql.SQLException;

import model.IMap;
import model.dao.ElementDAO;
import model.dao.MetadataDAO;
import model.Element.ElementFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class Map.
 */
public class Map implements IMap {

	/** The width. */
	private int width;

	/** The height. */
	private int height;

	/** The on the map. */
	private model.Element.Element[][] onTheMap;

	/** The score. */
	private int score = 0;

	/** The score needed. */
	private int scoreNeeded;

	/** The exit X. */
	private int exitX;

	/** The exit Y. */
	private int exitY;

	/** The win. */
	private boolean win = false;

	/** The lose. */
	private boolean lose = false;

	/** The table ID. */
	public int tableID = 2;

	/**
	 * Instantiates a new map.
	 *
	 * @throws SQLException the SQL exception
	 */
	public Map() throws SQLException {
		this.setWidth(MetadataDAO.getMapMetadataWidth(this.tableID));
		this.setHeight(MetadataDAO.getMapMetadataHeight(this.tableID));
		this.setExitX(MetadataDAO.getMapMetadataDoorX(this.tableID));
		this.setExitY(MetadataDAO.getMapMetadataDoorY(this.tableID));
		this.setScoreNeeded(MetadataDAO.getMapMetadataScoreNeed(this.tableID));
		this.onTheMap = new model.Element.Element[this.width][this.height];
		this.fillOnTheMap();
	}

	/**
	 * Fill on the map.
	 *
	 * @throws SQLException the SQL exception
	 */
	public void fillOnTheMap() throws SQLException {
		int x, y;
		int ID = this.tableID;
		for (y = 0; y < this.height; y++) {
			for (x = 0; x < this.width; x++) {
				this.setOnTheMapXY(ElementFactory.getObject(ElementDAO.getElement(ID, x, y)), x, y);
			}
		}
	}

	/**
	 * Spawn exit.
	 */
	@Override
	public void spawnExit() {
		this.setOnTheMapXY(ElementFactory.getObject("DOOR"), this.exitX, this.exitY);

	}

	/**
	 * Gets the on the map XY.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the on the map XY
	 */
	public model.Element.Element getOnTheMapXY(int x, int y) {

		return this.onTheMap[x][y];
	}

	/**
	 * Sets the on the map XY.
	 *
	 * @param element the element
	 * @param x       the x
	 * @param y       the y
	 */
	public void setOnTheMapXY(model.Element.Element element, int x, int y) {
		this.onTheMap[x][y] = element;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	@Override
	public int getWidth() {
		return this.width;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	@Override
	public int getHeight() {
		return this.height;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Gets the win.
	 *
	 * @return the win
	 */
	public boolean getWin() {
		return this.win;
	}

	/**
	 * Sets the win.
	 *
	 * @param win the new win
	 */
	public void setWin(boolean win) {
		this.win = win;
	}

	/**
	 * Gets the lose.
	 *
	 * @return the lose
	 */
	public boolean getLose() {
		return this.lose;
	}

	/**
	 * Sets the lose.
	 *
	 * @param lose the new lose
	 */
	public void setLose(boolean lose) {
		this.lose = lose;
	}

	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * Sets the score.
	 *
	 * @param score the new score
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Gets the score needed.
	 *
	 * @return the score needed
	 */
	public int getScoreNeeded() {
		return this.scoreNeeded;
	}

	/**
	 * Sets the score needed.
	 *
	 * @param scoreNeeded the new score needed
	 */
	public void setScoreNeeded(int scoreNeeded) {
		this.scoreNeeded = scoreNeeded;
	}

	/**
	 * Gets the exit X.
	 *
	 * @return the exit X
	 */
	public int getExitX() {
		return this.exitX;
	}

	/**
	 * Sets the exit X.
	 *
	 * @param exitX the new exit X
	 */
	public void setExitX(int exitX) {
		this.exitX = exitX;
	}

	/**
	 * Gets the exit Y.
	 *
	 * @return the exit Y
	 */
	public int getExitY() {
		return this.exitY;
	}

	/**
	 * Sets the exit Y.
	 *
	 * @param exitY the new exit Y
	 */
	public void setExitY(int exitY) {
		this.exitY = exitY;
	}

}

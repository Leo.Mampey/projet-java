/*
 *
 */
package model.Element;

import model.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Enemy.
 */
public class Enemy extends Living implements Moves {

	/** The sprite. */
	private static String SPRITE = "ENEMY.jpg";

	/** The direction. */
	private char direction = 'r';

	/**
	 * Instantiates a new enemy.
	 */
	public Enemy() {
		super(Enemy.SPRITE);

	}

	/**
	 * Destruction.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void destruction(int x, int y, Map map) {
		map.setOnTheMapXY(new Empty(), x, y);
	}

	/**
	 * Walk over.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void walkOver(int x, int y, char direction, Map map) {
		switch (direction) {

		case 'r':
			x = x + 1;

			break;

		case 'l':
			x = x - 1;

			break;

		case 'u':
			y = y - 1;

			break;

		case 'd':
			y = y + 1;
			;

			break;
		}
		this.death(x, y, map);
	}

	/**
	 * Death.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void death(int x, int y, Map map) {
		int i, j;

		for (i = 0; i < 3; i++) {
			for (j = 0; j < 3; j++) {
				map.getOnTheMapXY((x - 1) + j, (y - 1) + i).destruction((x - 1) + j, (y - 1) + i, map);
				if (map.getOnTheMapXY((x - 1) + j, (y - 1) + i).getSprite() == "VOID.jpg") {
					map.setOnTheMapXY(new Diamond(), (x - 1) + j, (y - 1) + i);
				}
			}
		}
	}

	/**
	 * Move.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void move(int x, int y, int direction, Map map) {

		if (this.walk == 0) {

			switch (direction) {

			case 0:
				this.moveRight(x, y, map);

				break;

			case 1:
				this.moveLeft(x, y, map);

				break;

			case 2:
				this.moveUp(x, y, map);

				break;

			case 3:
				this.moveDown(x, y, map);

				break;
			}
		}
	}

	/**
	 * Free place.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 * @return true, if successful
	 */
	public boolean freePlace(int x, int y, Map map) {
		// System.out.println(map.getOnTheMapXY(x, y).getSprite());
		if ((map.getOnTheMapXY(x, y).getSprite() == "VOID.jpg")
				|| (map.getOnTheMapXY(x, y).getSprite() == "PLAYER.jpg")) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Move up.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void moveUp(int x, int y, Map map) {
		if (this.freePlace(x, y - 1, map)) {
			map.getOnTheMapXY(x, y - 1).walkOver(x, y, 'u', map);
			this.walk = 1;
		} else {
			this.move(x, y, 3, map);
		}
	}

	/**
	 * Move down.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void moveDown(int x, int y, Map map) {
		if (this.freePlace(x, y + 1, map)) {
			map.getOnTheMapXY(x, y + 1).walkOver(x, y, 'd', map);
			this.walk = 1;
		} else {
			this.move(x, y, 0, map);
		}
	}

	/**
	 * Move left.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void moveLeft(int x, int y, Map map) {
		if (this.freePlace(x - 1, y, map)) {
			map.getOnTheMapXY(x - 1, y).walkOver(x, y, 'l', map);
			this.walk = 1;
		} else {
			this.move(x, y, 2, map);
		}
	}

	/**
	 * Move right.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void moveRight(int x, int y, Map map) {

		if (this.freePlace(x + 1, y, map)) {
			map.getOnTheMapXY(x + 1, y).walkOver(x, y, 'r', map);
			this.walk = 1;
		} else {
			this.move(x, y, 1, map);
		}
	}

}

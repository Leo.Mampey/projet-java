/*
 * 
 */
package model.Element;

// TODO: Auto-generated Javadoc
/**
 * The Class Living.
 */
public abstract class Living extends Element {

	/**
	 * Instantiates a new living.
	 *
	 * @param SPRITE the sprite
	 */
	public Living(String SPRITE) {
		super(SPRITE);
	}

	/**
	 * Death.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	public void death(int x, int y, model.Map map) {
	}

}
/*
 * 
 */
package model.Element;

import model.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Element.
 */
public abstract class Element implements IElement {

	/** The sprite. */
	String sprite;

	/** The walk. */
	int walk = 0;

	/**
	 * Instantiates a new element.
	 *
	 * @param sprite the sprite
	 */
	public Element(String sprite) {
		this.sprite = sprite;

	}

	/**
	 * Gets the sprite.
	 *
	 * @return the sprite
	 */
	@Override
	public String getSprite() {
		return this.sprite;
	}

	/**
	 * Destruction.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void destruction(int x, int y, Map map) {

	}

	/**
	 * Walk over.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void walkOver(int x, int y, char direction, Map map) {

	}

	/**
	 * Move.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void move(int x, int y, int direction, Map map) {

	}

	/**
	 * Checks if is walk.
	 *
	 * @return the int
	 */
	public int isWalk() {
		return this.walk;
	}

	/**
	 * Sets the walk.
	 *
	 * @param walk the new walk
	 */
	public void setWalk(int walk) {
		this.walk = walk;
	}

}
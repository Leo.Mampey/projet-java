/*
 * 
 */
package model.Element;

// TODO: Auto-generated Javadoc
/**
 * The Interface Moves.
 */
public interface Moves {

	/**
	 * Move up.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	void moveUp(int x, int y, model.Map map);

	/**
	 * Move down.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	void moveDown(int x, int y, model.Map map);

	/**
	 * Move left.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	void moveLeft(int x, int y, model.Map map);

	/**
	 * Move right.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	void moveRight(int x, int y, model.Map map);

}
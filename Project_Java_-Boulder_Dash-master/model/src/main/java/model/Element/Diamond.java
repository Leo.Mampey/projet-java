/*
 *
 */
package model.Element;

import model.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Diamond.
 */
public class Diamond extends Motion implements Moves {

	/** The sprite. */
	private static String SPRITE = "DIAMOND.jpg";

	/**
	 * Instantiates a new diamond.
	 */
	public Diamond() {
		super(Diamond.SPRITE);

	}

	/**
	 * Walk over.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void walkOver(int x, int y, char direction, Map map) {
		map.setScore(map.getScore() + 1);
		map.setOnTheMapXY(new Empty(), x, y);

		switch (direction) {
		case 'u':
			map.setOnTheMapXY(new Player(), x, y - 1);
			break;
		case 'd':
			map.setOnTheMapXY(new Player(), x, y + 1);
			break;
		case 'l':
			map.setOnTheMapXY(new Player(), x - 1, y);
			break;
		case 'r':
			map.setOnTheMapXY(new Player(), x + 1, y);
			break;

		}

	}

}

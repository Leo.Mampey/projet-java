/*
 *
 */
package model.Element;

import model.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Rock.
 */
public class Rock extends Motion implements Moves {

	/** The sprite. */
	private static String SPRITE = "ROCK.jpg";

	/**
	 * Instantiates a new rock.
	 */
	public Rock() {
		super(Rock.SPRITE);
	}

	/**
	 * Walk over.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void walkOver(int x, int y, char direction, Map map) {

		switch (direction) {

		case 'r':
			if ((map.getOnTheMapXY(x + 1, y + 1).getSprite() != "VOID.jpg")
					&& (map.getOnTheMapXY(x + 2, y).getSprite() == "VOID.jpg")) {
				map.getOnTheMapXY(x + 2, y).walkOver(x + 1, y, direction, map);
				map.getOnTheMapXY(x + 1, y).walkOver(x, y, direction, map);
			}
			break;

		case 'l':
			if ((map.getOnTheMapXY(x - 1, y + 1).getSprite() != "VOID.jpg")
					&& (map.getOnTheMapXY(x - 2, y).getSprite() == "VOID.jpg")) {
				map.getOnTheMapXY(x - 2, y).walkOver(x - 1, y, direction, map);
				map.getOnTheMapXY(x - 1, y).walkOver(x, y, direction, map);
			}
			break;

		}
	}

}
/*
 * 
 */
package model.Element;

// TODO: Auto-generated Javadoc
/**
 * The Interface IElement.
 */
public interface IElement {

	/**
	 * Gets the sprite.
	 *
	 * @return the sprite
	 */
	String getSprite();

	/**
	 * Destruction.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	void destruction(int x, int y, model.Map map);

	/**
	 * Walk over.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	void walkOver(int x, int y, char direction, model.Map map);

	/**
	 * Move.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	void move(int x, int y, int direction, model.Map map);

}
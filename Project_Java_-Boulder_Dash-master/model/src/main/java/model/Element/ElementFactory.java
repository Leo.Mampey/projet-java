/*
 *
 */
package model.Element;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating Element objects.
 */
public abstract class ElementFactory {

	/** The Rock. */
	public static Rock Rock = new Rock();

	/** The Empty. */
	public static Empty Empty = new Empty();

	/** The Enemy. */
	public static Enemy Enemy = new Enemy();

	/** The Exit. */
	public static Exit Exit = new Exit();

	/** The Ground. */
	public static Ground Ground = new Ground();

	/** The Player. */
	public static Player Player = new Player();

	/** The Wall. */
	public static Wall Wall = new Wall();

	/** The Diamond. */
	public static Diamond Diamond = new Diamond();

	/** The element. */
	private static Element[] element = { ElementFactory.Rock, ElementFactory.Diamond, ElementFactory.Enemy,
			ElementFactory.Player, ElementFactory.Empty, ElementFactory.Ground, ElementFactory.Exit,
			ElementFactory.Wall };

	/**
	 * Gets the object.
	 *
	 * @param object the object
	 * @return the object
	 */
	public static Element getObject(String object) {

		for (final Element Element : ElementFactory.element) {
			if (Element.getSprite().compareTo(object + ".jpg") == 0) {
				return Element;
			}
		}
		return ElementFactory.Empty;
	}

}

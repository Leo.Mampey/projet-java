/*
 *
 */
package model.Element;

import model.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Exit.
 */
public class Exit extends Element {

	/** The sprite. */
	private static String SPRITE = "DOOR.jpg";

	/**
	 * Instantiates a new exit.
	 */
	public Exit() {
		super(Exit.SPRITE);

	}

	/**
	 * Walk over.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void walkOver(int x, int y, char direction, Map map) {
		map.setWin(true);
	}

}
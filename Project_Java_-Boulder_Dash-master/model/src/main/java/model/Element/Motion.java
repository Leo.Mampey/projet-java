/*
 *
 */
package model.Element;

import model.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Motion.
 */
public abstract class Motion extends Element implements Moves {

	/** The movement. */
	boolean movement = false;

	/**
	 * Instantiates a new motion.
	 *
	 * @param sprite the sprite
	 */
	public Motion(String sprite) {
		super(sprite);

	}

	/**
	 * Free place.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 * @return true, if successful
	 */
	public boolean freePlace(int x, int y, Map map) {
		if ((map.getOnTheMapXY(x, y).getSprite() == "VOID.jpg")
				|| ((map.getOnTheMapXY(x, y).getSprite() == "PLAYER.jpg") && this.movement)
				|| ((map.getOnTheMapXY(x, y).getSprite() == "ENEMY.jpg") && this.movement)) {
			return true;

		} else {
			return false;
		}
	}

	/**
	 * Move.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void move(int x, int y, int direction, Map map) {
		// System.out.println(map.getOnTheMapXY(x, y+1).getSprite());
		if (this.freePlace(x, y + 1, map)) {
			this.moveDown(x, y, map);
			this.movement = true;
			// System.out.println(movement);

		} else {
			this.movement = false;

		}

		if (this.freePlace(x - 1, y, map) && this.freePlace(x - 1, y + 1, map)
				&& ((map.getOnTheMapXY(x, y + 1).getSprite() == "ROCK.jpg")
						|| (map.getOnTheMapXY(x, y + 1).getSprite() == "DIAMOND.jpg"))) {
			this.moveLeft(x, y, map);
		}

		if (this.freePlace(x + 1, y, map) && this.freePlace(x + 1, y + 1, map)
				&& ((map.getOnTheMapXY(x, y + 1).getSprite() == "ROCK.jpg")
						|| (map.getOnTheMapXY(x, y + 1).getSprite() == "DIAMOND.jpg"))) {
			this.moveRight(x, y, map);

		}

	}

	/**
	 * Destruction.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void destruction(int x, int y, Map map) {
		map.setOnTheMapXY(new Empty(), x, y);
	}

	/**
	 * Move up.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void moveUp(int x, int y, Map map) {
		// void
	}

	/**
	 * Move down.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void moveDown(int x, int y, Map map) {
		map.getOnTheMapXY(x, y + 1).walkOver(x, y, 'd', map);
	}

	/**
	 * Move left.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void moveLeft(int x, int y, Map map) {
		map.getOnTheMapXY(x + 1, y).walkOver(x, y, 'l', map);
	}

	/**
	 * Move right.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void moveRight(int x, int y, Map map) {
		map.getOnTheMapXY(x - 1, y).walkOver(x, y, 'r', map);
	}

}
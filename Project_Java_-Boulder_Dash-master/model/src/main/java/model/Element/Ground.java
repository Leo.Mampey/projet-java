/*
 *
 */
package model.Element;

import model.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Ground.
 */
public class Ground extends Element {

	/** The sprite. */
	private static String SPRITE = "SOIL.jpg";

	/**
	 * Instantiates a new ground.
	 */
	public Ground() {
		super(Ground.SPRITE);
	}

	/**
	 * Walk over.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void walkOver(int x, int y, char direction, Map map) {
		switch (direction) {
		case 'u':
			map.setOnTheMapXY(map.getOnTheMapXY(x, y), x, y - 1);
			break;
		case 'd':
			map.setOnTheMapXY(map.getOnTheMapXY(x, y), x, y + 1);
			break;
		case 'l':
			map.setOnTheMapXY(map.getOnTheMapXY(x, y), x - 1, y);
			break;
		case 'r':
			map.setOnTheMapXY(map.getOnTheMapXY(x, y), x + 1, y);
			break;
		}
		map.setOnTheMapXY(new Empty(), x, y);

	}

	/**
	 * Destruction.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void destruction(int x, int y, Map map) {
		map.setOnTheMapXY(new Empty(), x, y);
	}
}
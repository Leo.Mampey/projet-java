/*
 *
 */
package model.Element;

import model.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Player.
 */
public class Player extends Living {

	/** The sprite. */
	private static String SPRITE = "PLAYER.jpg";

	/**
	 * Instantiates a new player.
	 */
	public Player() {
		super(Player.SPRITE);

	}

	/**
	 * Destruction.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void destruction(int x, int y, Map map) {
		this.death(x, y, map);
	}

	/**
	 * Death.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	@Override
	public void death(int x, int y, Map map) {
		map.setLose(true);
	}

	/**
	 * Walk over.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void walkOver(int x, int y, char direction, Map map) {
		this.death(x, y, map);

	}

	/**
	 * Move.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param direction the direction
	 * @param map       the map
	 */
	@Override
	public void move(int x, int y, int direction, Map map) {
		switch (direction) {
		case 38:
			this.moveUp(x, y, map);
			break;
		case 40:
			this.moveDown(x, y, map);
			break;
		case 39:
			this.moveRight(x, y, map);
			break;
		case 37:
			this.moveLeft(x, y, map);
			break;
		}
	}

	/**
	 * Move up.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	public void moveUp(int x, int y, Map map) {
		map.getOnTheMapXY(x, y - 1).walkOver(x, y, 'u', map);

	}

	/**
	 * Move down.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	public void moveDown(int x, int y, Map map) {
		map.getOnTheMapXY(x, y + 1).walkOver(x, y, 'd', map);

	}

	/**
	 * Move left.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	public void moveLeft(int x, int y, Map map) {
		map.getOnTheMapXY(x - 1, y).walkOver(x, y, 'l', map);

	}

	/**
	 * Move right.
	 *
	 * @param x   the x
	 * @param y   the y
	 * @param map the map
	 */
	public void moveRight(int x, int y, Map map) {
		map.getOnTheMapXY(x + 1, y).walkOver(x, y, 'r', map);

	}

}

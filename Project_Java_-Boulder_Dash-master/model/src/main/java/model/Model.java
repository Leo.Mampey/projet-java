/*
 * 
 */
package model;

import java.sql.SQLException;

// TODO: Auto-generated Javadoc
/**
 * The Class Model.
 */
public class Model {

	/** The map. */
	private Map map;

	/**
	 * Instantiates a new model.
	 *
	 * @throws SQLException the SQL exception
	 */
	public Model() throws SQLException {
		this.map = new Map();

	}

	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	public Map getMap() {
		return this.map;

	}

	/**
	 * Sets the map.
	 *
	 * @param map the new map
	 */
	public void setMap(Map map) {
		this.map = map;
	}

}

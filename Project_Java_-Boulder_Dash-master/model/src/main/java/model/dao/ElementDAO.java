/*
 * 
 */
package model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class ElementDAO.
 */
public abstract class ElementDAO extends AbstractDAO {

	/** The sql map element lv 1. */
	private static String sqlMapElementLv1 = "{call findMapElementLv1(?)}";

	/** The sql map element lv 2. */
	private static String sqlMapElementLv2 = "{call findMapElementLv2(?)}";

	/** The sql map element lv 3. */
	private static String sqlMapElementLv3 = "{call findMapElementLv3(?)}";

	/** The sql map element lv 4. */
	private static String sqlMapElementLv4 = "{call findMapElementLv4(?)}";

	/** The sql map element lv 5. */
	private static String sqlMapElementLv5 = "{call findMapElementLv5(?)}";

	/**
	 * Gets the map element lv 1.
	 *
	 * @param row the row
	 * @return the map element lv 1
	 * @throws SQLException the SQL exception
	 */
	private static ArrayList<String> getMapElementLv1(int row) throws SQLException {
		row += 1;
		final ArrayList<String> elementsList = new ArrayList<String>();
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(ElementDAO.sqlMapElementLv1);
		callStatement.setInt(1, row);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				for (int i = 1; i < 11; i++) {
					elementsList.add(result.getString(i + 1));
				}
			}
			result.close();
		}
		// System.out.println(elementsList);
		return elementsList;
	}

	/**
	 * Gets the map element lv 2.
	 *
	 * @param row the row
	 * @return the map element lv 2
	 * @throws SQLException the SQL exception
	 */
	private static ArrayList<String> getMapElementLv2(int row) throws SQLException {
		row += 1;
		final ArrayList<String> elementsList = new ArrayList<String>();
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(ElementDAO.sqlMapElementLv2);
		callStatement.setInt(1, row);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				for (int i = 1; i < 16; i++) {
					elementsList.add(result.getString(i + 1));
				}
			}
			result.close();
		}
		return elementsList;
	}

	/**
	 * Gets the map element lv 3.
	 *
	 * @param row the row
	 * @return the map element lv 3
	 * @throws SQLException the SQL exception
	 */
	private static ArrayList<String> getMapElementLv3(int row) throws SQLException {
		row += 1;
		final ArrayList<String> elementsList = new ArrayList<String>();
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(ElementDAO.sqlMapElementLv3);
		callStatement.setInt(1, row);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				for (int i = 1; i < 21; i++) {
					elementsList.add(result.getString(i + 1));
				}
			}
			result.close();
		}
		return elementsList;
	}

	/**
	 * Gets the map element lv 4.
	 *
	 * @param row the row
	 * @return the map element lv 4
	 * @throws SQLException the SQL exception
	 */
	private static ArrayList<String> getMapElementLv4(int row) throws SQLException {
		row += 1;
		final ArrayList<String> elementsList = new ArrayList<String>();
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(ElementDAO.sqlMapElementLv4);
		callStatement.setInt(1, row);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				for (int i = 1; i < 31; i++) {
					elementsList.add(result.getString(i + 1));
				}
				result.close();
			}
		}
		return elementsList;
	}

	/**
	 * Gets the map element lv 5.
	 *
	 * @param row the row
	 * @return the map element lv 5
	 * @throws SQLException the SQL exception
	 */
	private static ArrayList<String> getMapElementLv5(int row) throws SQLException {
		row += 1;
		final ArrayList<String> elementsList = new ArrayList<String>();
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(ElementDAO.sqlMapElementLv5);
		callStatement.setInt(1, row);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				for (int i = 1; i < 41; i++) {
					elementsList.add(result.getString(i + 1));
				}
				result.close();
			}
		}
		return elementsList;
	}

	/**
	 * Gets the element.
	 *
	 * @param idLevel the id level
	 * @param column  the column
	 * @param row     the row
	 * @return the element
	 * @throws SQLException the SQL exception
	 */
	public static String getElement(int idLevel, int column, int row) throws SQLException {
		// System.out.println(column);
		String element = "";
		// System.out.println(column);
		ArrayList<String> elementsRow = new ArrayList<String>();
		switch (idLevel) {
		case 1:
			elementsRow = ElementDAO.getMapElementLv1(row);
			break;
		case 2:
			elementsRow = ElementDAO.getMapElementLv2(row);
			break;
		case 3:
			elementsRow = ElementDAO.getMapElementLv3(row);
			break;
		case 4:
			elementsRow = ElementDAO.getMapElementLv4(row);
			break;
		case 5:
			elementsRow = ElementDAO.getMapElementLv5(row);
			break;
		}
		int elementsRowSize = elementsRow.size();
		if (column <= elementsRowSize) {
			element = elementsRow.get(column);
		}
		// System.out.println(element);
		return element;
	}

}

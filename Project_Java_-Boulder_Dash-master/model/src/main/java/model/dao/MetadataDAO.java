/*
 * 
 */
package model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

// TODO: Auto-generated Javadoc
/**
 * The Class MetadataDAO.
 */
public abstract class MetadataDAO extends AbstractDAO {

	/** The sql map metadata height. */
	private static String sqlMapMetadataHeight = "{call findMapMetadataHeight(?)}";

	/** The sql map metadata width. */
	private static String sqlMapMetadataWidth = "{call findMapMetadataWidth(?)}";

	/** The sql map metadata diamonds needed. */
	private static String sqlMapMetadataDiamondsNeeded = "{call findMapMetadataDiamondsNeeded(?)}";

	/** The sql map metadata door X. */
	private static String sqlMapMetadataDoorX = "{call findMapMetadataDoorX(?)}";

	/** The sql map metadata door Y. */
	private static String sqlMapMetadataDoorY = "{call findMapMetadataDoorY(?)}";

	/**
	 * Gets the map metadata height.
	 *
	 * @param idLevel the id level
	 * @return the map metadata height
	 * @throws SQLException the SQL exception
	 */
	public static int getMapMetadataHeight(final int idLevel) throws SQLException {
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(MetadataDAO.sqlMapMetadataHeight);
		int height = 0;
		callStatement.setInt(1, idLevel);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				if (result.first()) {
					height = result.getInt(1);
				}
			}
			result.close();
		}
		return height;
	}

	/**
	 * Gets the map metadata width.
	 *
	 * @param idLevel the id level
	 * @return the map metadata width
	 * @throws SQLException the SQL exception
	 */
	public static int getMapMetadataWidth(final int idLevel) throws SQLException {
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(MetadataDAO.sqlMapMetadataWidth);
		int width = 0;
		callStatement.setInt(1, idLevel);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				if (result.first()) {
					width = result.getInt(1);
				}
			}
			result.close();
		}
		return width;
	}

	/**
	 * Gets the map metadata score need.
	 *
	 * @param idLevel the id level
	 * @return the map metadata score need
	 * @throws SQLException the SQL exception
	 */
	public static int getMapMetadataScoreNeed(final int idLevel) throws SQLException {
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(MetadataDAO.sqlMapMetadataDiamondsNeeded);
		int diamondsNeeded = 0;
		callStatement.setInt(1, idLevel);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				if (result.first()) {
					diamondsNeeded = result.getInt(1);
				}
			}
			result.close();
		}
		return diamondsNeeded;
	}

	/**
	 * Gets the map metadata door X.
	 *
	 * @param idLevel the id level
	 * @return the map metadata door X
	 * @throws SQLException the SQL exception
	 */
	public static int getMapMetadataDoorX(final int idLevel) throws SQLException {
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(MetadataDAO.sqlMapMetadataDoorX);
		int doorX = 0;
		callStatement.setInt(1, idLevel);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				if (result.first()) {
					doorX = result.getInt(1);
				}
			}
			result.close();
		}
		return doorX;
	}

	/**
	 * Gets the map metadata door Y.
	 *
	 * @param idLevel the id level
	 * @return the map metadata door Y
	 * @throws SQLException the SQL exception
	 */
	public static int getMapMetadataDoorY(final int idLevel) throws SQLException {
		final java.sql.CallableStatement callStatement = AbstractDAO.prepareCall(MetadataDAO.sqlMapMetadataDoorY);
		int doorY = 0;
		callStatement.setInt(1, idLevel);
		if (callStatement.execute()) {
			final ResultSet result = callStatement.getResultSet();
			if (result.next()) {
				if (result.first()) {
					doorY = result.getInt(1);
				}
			}
			result.close();
		}
		return doorY;
	}
}

/*
 *
 */
package controller;

import java.io.IOException;

import model.*;
import view.View;

// TODO: Auto-generated Javadoc
/**
 * The Class Controller.
 */
public class Controller {

	/** The model. */
	private Model model;

	/** The view. */
	private View view;

	/** The num. */
	private int num = 0;

	/**
	 * Play.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void play() throws IOException {
		this.view.show(this.model.getMap().getWidth(), this.model.getMap().getHeight());
		while (this.model.getMap().getWin() == false) {
			this.view.refresh(this.model, this.model.getMap().getWidth(), this.model.getMap().getHeight());
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.num = this.view.KeyUser();
			for (int y = this.model.getMap().getHeight() - 1; y >= 0; y--) {
				for (int x = 0; x < this.model.getMap().getWidth(); x++) {

					if (this.model.getMap().getOnTheMapXY(x, y).getSprite() == "PLAYER.jpg") {
						this.model.getMap().getOnTheMapXY(x, y).move(x, y, this.num, this.model.getMap());
						this.num = 0;
					} else {
						this.model.getMap().getOnTheMapXY(x, y).move(x, y, (int) (Math.random() * (0 - 3)),
								this.model.getMap());
					}

					if (this.model.getMap().getScoreNeeded() == this.model.getMap().getScore()) {
						if (this.model.getMap()
								.getOnTheMapXY(this.model.getMap().getExitX(), this.model.getMap().getExitY())
								.getSprite() == "PLAYER.jpg") {
							this.model.getMap().setWin(true);
						}
						this.model.getMap().spawnExit();
					}
				}

			}

			for (int y = 0; y < this.model.getMap().getHeight(); y++) {
				for (int x = 0; x < this.model.getMap().getWidth(); x++) {
					this.model.getMap().getOnTheMapXY(x, y).setWalk(0);
				}
			}

			for (int y = 0; y < this.model.getMap().getHeight(); y++) {
				for (int x = 0; x < this.model.getMap().getWidth(); x++) {

					switch (this.model.getMap().getOnTheMapXY(x, y).getSprite()) {
					case "ROCK.jpg":
						System.out.print("O ");
						break;

					case "BORDER.jpg":
						System.out.print("[]");
						break;

					case "DIAMOND.jpg":
						System.out.print("V ");
						break;

					case "DOOR.jpg":
						System.out.print("D ");
						break;

					case "SOIL.jpg":
						System.out.print("* ");
						break;

					case "PLAYER.jpg":
						System.out.print("P ");
						break;

					case "ENEMY.jpg":
						System.out.print("X ");
						break;

					case "VOID.jpg":
						System.out.print("_ ");
						break;
					}
				}
				System.out.println("");

			}
			System.out.println("");

		}
		if (this.model.getMap().getLose() == false) {
			System.out.println("Success");
		} else {
			System.out.println("Loose");
		}
	}

	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public Model getModel() {
		return this.model;
	}

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public View getView() {
		return this.view;
	}

	/**
	 * Instantiates a new controller.
	 *
	 * @param view  the view
	 * @param model the model
	 */
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}

	/**
	 * Sets the model.
	 *
	 * @param model the new model
	 */
	public void setModel(Model model) {
		this.model = model;
	}

	/**
	 * Sets the view.
	 *
	 * @param view the new view
	 */
	public void setView(View view) {
		this.view = view;
	}

}
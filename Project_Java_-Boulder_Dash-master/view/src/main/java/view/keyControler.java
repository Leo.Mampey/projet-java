/*
 *
 */
package view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JLabel;

// TODO: Auto-generated Javadoc
/**
 * The Class keyControler.
 */
public class keyControler implements KeyListener {

	/** The key user. */
	private static int keyUser = 0;

	/** The label. */
	private JLabel label;

	/**
	 * Titre key listener.
	 *
	 * @param label_ the label
	 */
	public void TitreKeyListener(JLabel label_) {
		this.label = label_;
		System.out.println("key listener");
	}

	/**
	 * Key pressed.
	 *
	 * @param e the e
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		keyControler.keyUser = e.getKeyCode();
		// System.out.println("key user ");
		// System.out.println(keyUser);
	}

	/**
	 * Key released.
	 *
	 * @param e the e
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		keyControler.keyUser = 0;
	}

	/**
	 * Key typed.
	 *
	 * @param e the e
	 */
	@Override
	public void keyTyped(KeyEvent e) {
	}

	/**
	 * Gets the key user.
	 *
	 * @return the key user
	 */
	public static int getKeyUser() {
		return keyControler.keyUser;
	}

	/**
	 * Sets the key user.
	 *
	 * @param keyUser the new key user
	 */
	public void setKeyUser(int keyUser) {
		keyControler.keyUser = keyUser;
	}

}
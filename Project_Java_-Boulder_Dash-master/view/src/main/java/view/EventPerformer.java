/*
 *
 */
package view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;

// TODO: Auto-generated Javadoc
/**
 * The Class EventPerformer.
 */
public class EventPerformer extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Key code to user.
	 */
	public void keyCodeToUser() {
		JLabel label = new JLabel("tape pour test");
		this.add(label, BorderLayout.CENTER);

		this.addKeyListener(new keyControler());

		this.setPreferredSize(new Dimension(640, 640));
		this.pack();
	}
}

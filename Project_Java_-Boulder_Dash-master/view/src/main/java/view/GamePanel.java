/*
 *
 */
package view;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JFrame;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class GamePanel.
 *
 * @param <mainFrame> the generic type
 */
public class GamePanel<mainFrame> extends JPanel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The sprite name. */
	private String spriteName;

	/** The score. */
	private int score;

	/** The image. */
	private Image image;

	/** The main frame. */
	private JFrame mainFrame;

	/**
	 * Instantiates a new game panel.
	 *
	 * @param image the image
	 */
	public GamePanel(Image image) {
		this.image = image;
		this.mainFrame = View.getMainFrame();
		// System.out.println("construteur gamepanel");
	}

	/**
	 * Paint component before.
	 */
	public void paintComponentBefore() {
		this.paintComponent(this.getGraphics());
	}

	/**
	 * Paint component.
	 *
	 * @param g the g
	 */
	@Override
	protected void paintComponent(Graphics g) {

		// System.out.println("yo paint component");

		g.drawImage(this.image, 0, 0, View.getMainFrame());
		this.setVisible(true);
	}
}
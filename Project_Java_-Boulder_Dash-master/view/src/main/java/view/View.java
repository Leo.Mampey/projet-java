/*
 *
 */
package view;// TODO: Auto-generated Javadoc

import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import model.Model;

// TODO: Auto-generated Javadoc
/**
 * The Class View.
 */
public class View extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The model. */
	private Model model;

	/** The main frame. */
	private static JFrame mainFrame;

	/** The game panel. */
	private GamePanel gamePanel;

	/**
	 * Instantiates a new view.
	 *
	 * @param model the model
	 */
	public View(Model model) {
		this.model = model;
	}

	/**
	 * Show.
	 *
	 * @param x the x
	 * @param y the y
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void show(int x, int y) throws IOException {
		View.mainFrame = new JFrame();
		this.setTitle("BoulderDash - The Game");
		this.setSize(x * 33, y * 33);
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(new FontPanel(this.model, x, y));
		this.addKeyListener(new keyControler());

		ImageIcon img = new ImageIcon("../ressources/icon.png");
		this.setIconImage(img.getImage());

		// System.out.println(x);
		// System.out.println(y);
	}

	/**
	 * Refresh.
	 *
	 * @param model the model
	 * @param x     the x
	 * @param y     the y
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void refresh(Model model, int x, int y) throws IOException {
		this.setContentPane(new FontPanel(model, x, y));
		this.addKeyListener(new keyControler());
		this.setVisible(true);
	}

	/*
	 * public void showPanel (int width, int height, String sprite,int score) {
	 * GamePanel pan; try { pan = new GamePanel(sprite, width, height, score );
	 * pan.paintComponentBefore();
	 *
	 *
	 * } catch (IOException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); }
	 *
	 * //System.out.println(sprite); }
	 */

	/**
	 * Key user.
	 *
	 * @return the int
	 */
	public int KeyUser() {
		return keyControler.getKeyUser();
	}

	/**
	 * Gets the main frame.
	 *
	 * @return the main frame
	 */
	public static JFrame getMainFrame() {
		return View.mainFrame;
	}

	/**
	 * Sets the main frame.
	 *
	 * @param mainFrame the new main frame
	 */
	public void setMainFrame(JFrame mainFrame) {
		View.mainFrame = mainFrame;
	}

}
/*
 *
 */
package contract;

// TODO: Auto-generated Javadoc
/**
 * The Enum ControllerOrder.
 */

public enum ControllerOrder {

	/** The up. */
	UP,

	/** The down. */
	DOWN,

	/** The left. */
	LEFT,

	/** The right. */
	RIGHT
}

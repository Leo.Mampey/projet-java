/*
 * 
 */
package main;

import java.io.IOException;
import java.sql.SQLException;

import controller.Controller;
import model.Model;
import view.View;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public abstract class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws SQLException the SQL exception
	 * @throws IOException  Signals that an I/O exception has occurred.
	 */
	public static void main(final String[] args) throws SQLException, IOException {
		Model model = new Model();
		View view = new View(model);
		Controller ctrl = new Controller(view, model);
		ctrl.play();
	}
}
